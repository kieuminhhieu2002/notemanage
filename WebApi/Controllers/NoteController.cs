﻿using Application.Interfaces;
using Domain.Entities;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    public class NoteController : BaseController
    {
        private readonly INoteService _noteService;

        public NoteController(INoteService noteService)
        {
            _noteService = noteService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllNotes()
        {

            try
            {
                var listNote = await _noteService.GetAllNotes();

                return Ok(listNote);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> CreateNote([FromForm] NoteViewModel noteViewModel)
        {
            try
            {
                var check = await _noteService.CreateNote(noteViewModel);
                if (check!=null)
                {
                    return CreatedAtAction("GetNoteById", new { check.Id },check);
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
        }

        [HttpGet("/GetNoteById")]
        public async Task<IActionResult> GetNoteById(Guid id)
        {
            try
            {
                var note = await _noteService.GetNote(id);
                if (note == null)
                {
                    return BadRequest();
                }
                return Ok(note);
            }
            catch(Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteNote(Guid id)
        {
            try
            {
                var check= await _noteService.DeleteNote(id);
                if(check)
                {
                    return Ok();
                }
                return BadRequest();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
        }

        [HttpGet("/ExportToExcel")]
        public async Task<IActionResult> ExportToExcel()
        {
            var check = await _noteService.ExportToExcel();
            if (check)
            {
                return Ok("Successfully!");
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateNote([FromForm] NoteUpdateModel noteUpdate)
        {
            //try
            //{
                var check= await _noteService.UpdateNote(noteUpdate);
                if (check)
                {
                    return CreatedAtAction("GetNoteById", new { noteUpdate.Id });
                }
                return BadRequest();
            //}
            //catch (Exception)
            //{
            //    return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            //}
        }
    }
}
