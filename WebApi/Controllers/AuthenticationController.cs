﻿using Application.Interface;
using Global.Shared.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace WebApi.Controllers
{
    public class AuthenticationController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService= authenticationService;
        }


        [HttpPost]
        public async Task<IActionResult> Login(string userName, string password)
        {
            try
            {
                var token = await _authenticationService.Login(userName, password);
                if (token == null)
                {
                    return NotFound("Incorrect UserName & Password!");
                }
                return Ok(token);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
           
        }

        [HttpPost("/Refresh-Token")]
        public async Task<IActionResult> RefreshToken(TokenModel tokenModel)
        {
            try
            {
                var result = await _authenticationService.RefreshToken(tokenModel);
                if (result == null)
                {
                    return BadRequest();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
           
        }


        [Authorize]
        [HttpPost("/Logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                var logout = await _authenticationService.Logout();
                if (logout)
                {
                    return NoContent();
                }

                return Unauthorized();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
            
        }
    }
}
