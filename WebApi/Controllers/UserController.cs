﻿using Application.Interface;
using Application.Interfaces;
using Domain.Entities;
using Global.Shared.Common;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebApi.Service;

namespace WebApi.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IClaimsService _claimService;
        public UserController(IUserService userService, IClaimsService claimService)
        {
            _userService = userService;
            _claimService = claimService;
        }


        [HttpPost]
        public async Task<ActionResult> Register([FromForm] UserViewModel userView)
        {
            try
            {
                var message = await _userService.Register(userView);
                if (message == false)
                {
                    return BadRequest();
                }
                return Ok("Register successfully!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
            
        }

        [HttpGet("/GetAllUser")]
        public async Task<IActionResult> GetAllUser()
        {
            try
            {
                var users= await _userService.GetListUserAsync();
                var list = from u in users
                           select new
                           {
                               Id = u.Id,
                               UserName = u.UserName,
                               LastAccesseddate = u.LastAccessedDate,
                               NumberOfNote = u.Notes.Count,
                               CreatedDate = u.CreatedDate,
                               avatarURL = u.AvatarURL,
                               RoleId= u.RoleID,
                               Role= u.Role.RoleName,
                               Status=u.Status
                           };
                return Ok(list);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
        }

        [HttpPut("/Block-Account")]
        public async Task<IActionResult> BlockAccount(Guid id)
        {
            try
            {
                var check = await _userService.BlockAccount(id);
                if (check == false)
                {
                    return BadRequest();
                }
                return Ok("Successfully!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
        }

        [HttpPut("/UpdateToAdmin")]
        public async Task<IActionResult> UpdateToAdmin(Guid id)
        {
            try
            {
                var check = await _userService.UpdateToAdmin(id);
                if (check == false)
                {
                    return BadRequest();
                }
                return Ok("Successfully!");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            }
        }

        [HttpPut("/UploadImage")]
        public async Task<IActionResult> UploadImage(Guid id,IFormFile file)
        {
            //try
            //{
                var check = await _userService.UpLoadFile(id,file);
                if (check == false)
                {
                    return BadRequest();
                }
                return Ok("Successfully!");
            //}
            //catch (Exception)
            //{
            //    return StatusCode(StatusCodes.Status500InternalServerError, "Server Error!");
            //}
        }



        [Authorize]
        [HttpGet]
        public async Task<ActionResult> TestClaimId()
        {
            

            return Ok(_claimService.GetCurrentUserId);
        }


    }
}
