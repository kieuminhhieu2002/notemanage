using Application.Interface;
using Global.Shared.MyClasss;
using Hangfire;
using Infrastructures;
using WebApi;
using WebApi.Middlewares;
using WebApi.Service;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddInfrastructuresService();
builder.Services.AddWebService();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseMiddleware<GlobalExceptionMiddleware>();
app.UseMiddleware<PerformanceMiddleware>();

app.UseHttpsRedirection();


app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseHangfireDashboard();
app.MapHangfireDashboard();

await app.StartAsync();
RecurringJob.AddOrUpdate<EmailService>(e => e.SendToUser(), "0 1 * * MON-SUN");
await app.WaitForShutdownAsync();

app.Run();
