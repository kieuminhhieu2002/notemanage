﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Application.Repositories;

namespace Global.Shared.MyClasss
{
    public class EmailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EmailService> _logger;
        private readonly IConfiguration _configuration;

        public EmailService(IUnitOfWork unitOfWork, ILogger<EmailService> logger, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _configuration = configuration;
        }

        public async Task SendToUser()
        {
            var message = "Xin Chao, chuc ban buoi sang vui ve";
            var listUser = await _unitOfWork.UserRepo.GetAllAsync();
           
                await SendEmail("minhhieutb321@gmail.com", "Daily Email", message);
            
        }

        public async Task SendEmail(string emailTo, string subject, string message)
        {
            try
            {
                var _email = "Hey";
                var _epass = "Hey";
                var _dispName = "TestMail";
                MailMessage myMessage = new MailMessage();
                myMessage.To.Add(emailTo);
                myMessage.From = new MailAddress(_email, _dispName);
                myMessage.Subject = subject;
                myMessage.Body = message;
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.EnableSsl = true;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(_email, _epass);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
                    await smtp.SendMailAsync(myMessage);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }

}
