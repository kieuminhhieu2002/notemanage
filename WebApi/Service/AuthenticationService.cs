﻿ using Application.Interface;
using Application.Repositories;
using Application.Utils;
using Domain.Entities;
using Global.Shared.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WebApi.Service;

namespace WebApi.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _config;
        private readonly IClaimsService _claimsService;
        private string key;
        private string issuer;
        private string audience;
        private string expireTime;

        public AuthenticationService(IUnitOfWork unitOfWork, IConfiguration config, IClaimsService claimsService)
        {
            _unitOfWork = unitOfWork;
            _config = config;
            _claimsService = claimsService;
            key = _config["JWT:Key"];
            issuer = _config["JWT:Issuer"];
            audience = _config["JWT:Audience"];
            expireTime = _config["JWT:TokenValidityInMinutes"];
        }
        

        public async Task<TokenModel> Login(string userName, string password)
        {
            User user = await _unitOfWork.UserRepo.GetUserLoginAsync(userName, password);
            if (user != null)
            {
                

                var token = user.GenerateJSONWebToken(key,issuer,audience,expireTime);
                var refreshToken = GenerateRefreshToken.GetRefreshToken();

                _ = int.TryParse(_config["JWT:RefreshTokenValidityInDays"], out int RefreshTokenValidityInDays);

                DateTime expriedDate = DateTime.Now.AddDays(RefreshTokenValidityInDays);
                user.RefreshToken = refreshToken;
                user.RefreshTokenExpiryTime = expriedDate;
                user.LastAccessedDate = DateTime.Now;

                _unitOfWork.UserRepo.Update(user);
                await _unitOfWork.SaveChangeAsync();
                return new TokenModel
                {
                    AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                    RefreshToken = refreshToken
                };
            }
            return null;
        }

        public async Task<bool> Logout()
        {
            Guid rawUserID =  _claimsService.GetCurrentUserId;
            User userLogin = await _unitOfWork.UserRepo.GetByIdAsync(rawUserID,x=>x.Role);
            if (userLogin == null)
            {
                return false;
            }
            userLogin.RefreshToken = null;
            userLogin.RefreshTokenExpiryTime= null;
            _unitOfWork.UserRepo.Update(userLogin);
            bool check = await _unitOfWork.SaveChangeAsync()>0;
            
            return check;
        }

        public async Task<TokenModel> RefreshToken(TokenModel tokenModel)
        {
            if (tokenModel == null)
            {
                return null;
            }

            string? accessToken = tokenModel.AccessToken;
            string? refreshToken = tokenModel.RefreshToken;

            var principal = accessToken.GetPrincipalFromExpiredToken(key);
            if (principal == null)
            {
                return null;
            }
            var id = principal.FindFirstValue("userID");
            _ = Guid.TryParse(id, out Guid userID);
            var userLogin = await _unitOfWork.UserRepo.GetByIdAsync(userID, x => x.Role);
            if (userLogin == null || userLogin.RefreshToken != refreshToken || userLogin.RefreshTokenExpiryTime <= DateTime.Now)
            {
                return null;
            }

            var newAccessToken = userLogin.GenerateJSONWebToken(key, issuer, audience, expireTime);
            var newRefreshToken = GenerateRefreshToken.GetRefreshToken();

            userLogin.RefreshToken = newRefreshToken;
            _unitOfWork.UserRepo.Update(userLogin);
            await _unitOfWork.SaveChangeAsync();

            return new TokenModel
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(newAccessToken),
                RefreshToken = newRefreshToken
            };
        }
    }
}
