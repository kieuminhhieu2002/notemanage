﻿using Application.Interface;
using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using System.Text;
using System.Text.Json.Serialization;
using WebApi.Middlewares;
using WebApi.Service;
using WebApi.Services;

namespace WebApi
{
    public static class DependencyInjection
    {
        private static IConfiguration _config;
        public static IServiceCollection AddWebService(this IServiceCollection services)
        {
            services.AddHealthChecks();
            services.AddSingleton<GlobalExceptionMiddleware>();
            services.AddSingleton<PerformanceMiddleware>();
            services.AddSingleton<Stopwatch>();
            services.AddScoped<IClaimsService, ClaimService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddHttpContextAccessor();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder.WithOrigins("https://localhost:7009")
                        .SetIsOriginAllowedToAllowWildcardSubdomains()
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .Build();
                    });
            });
            services.AddControllers().AddJsonOptions(x =>
              x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);


            services.AddHangfire(config => config
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseInMemoryStorage()
                 );
            services.AddHangfireServer();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer=true,
                        ValidateAudience=true,
                        ValidateLifetime=true,
                        ValidateIssuerSigningKey=true,
                        //ValidIssuer = _config["JWT:Issuer"],
                        //ValidAudience = _config["JWT:Audience"],
                        //IssuerSigningKey=new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWT:Key"])),
                        ValidIssuer = "JWTAuthenticationHIGHsecuredPasswordVVVp1OH7Xzyr",
                        ValidAudience = "JWTAuthenticationHIGHsecuredPasswordVVVp1OH7Xzyr",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("JWTAuthenticationHIGHsecuredPasswordVVVp1OH7Xzyr")),
                        ClockSkew =TimeSpan.FromSeconds(1)
                    };
                });
            return services;
        }
    }
}
