﻿using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Tests.Mappers
{
    public class AutoMapperProfileTests:SetupTest
    {
        [Fact]
        public void TestUserMapper()
        {
            //arrange
            var userMock = _fixture.Build<User>().Without(u=>u.Notes).Create();

            //Act
            var result= _mapperConfig.Map<UserViewModel>(userMock);

            //assert
            result.UserName.Should().Be(userMock.UserName);
        }

        [Fact]
        public void TestNoteViewMapper()
        {
            //arrange
            
            var noteMock = _fixture.Build<Note>().Without(n=>n.User).Create();

            //Act
            var result = _mapperConfig.Map<NoteViewModel>(noteMock);

            //assert
            result.Title.Should().Be(noteMock.Title);
        }

        [Fact]
        public void TestNoteUpdateMapper()
        {
            //arrange
            var noteUpdateMock = _fixture.Build<Note>().Without(n => n.User).Create();

            //Act
            var result = _mapperConfig.Map<NoteUpdateModel>(noteUpdateMock);

            //assert
            result.Id.Should().Be(noteUpdateMock.Id);
        }
    }
}
