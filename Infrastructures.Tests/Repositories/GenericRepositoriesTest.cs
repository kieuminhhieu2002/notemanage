﻿using Application.Repositories;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Infrastructures.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Tests.Repositories
{
    public class GenericRepositoriesTest:SetupTest
    {
        private readonly IGenericRepository<User> _genericRepository;
        public GenericRepositoriesTest()
        {
            _genericRepository=new GenericRepository<User>(
                _appDbContext,
                _claimsServiceMock.Object,
                _currentTimeMock.Object      
                );
        }

        [Fact]
        public async Task GenericRepository_GetAllAsync_ShouldReturnCorrectData()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u=>u.Role).CreateMany(10).ToList();
            foreach(var item in mockData )
            {
                item.RoleID = 2;
            }

            await _appDbContext.Users.AddRangeAsync(mockData);
            
            await _appDbContext.SaveChangesAsync();

            var result = await _genericRepository.GetAllAsync();

            result.Should().BeEquivalentTo(mockData);
        }

        [Fact]
        public async Task GenericRepository_GetAllAsync_ShouldReturnEmptyWhenHaveNoData()
        {

            var result = await _genericRepository.GetAllAsync();

            result.Should().BeEmpty();
        }

        [Fact]
        public async Task GenericRepository_GetByIdAsync_ShouldReturnCorrectData()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u => u.Role).Create();
            await _appDbContext.Users.AddAsync(mockData);
            await _appDbContext.SaveChangesAsync();
            var result = await _genericRepository.GetByIdAsync(mockData.Id);

            result.Id.Should().Be(mockData.Id);
        }

        [Fact]
        public async Task GenericRepository_GetByIdAsync_ShouldReturnEmptyWhenHaveNoData()
        {

            var result = await _genericRepository.GetByIdAsync(Guid.Empty);

            result.Should().BeNull();
        }

        [Fact]
        public async Task GenericRepository_FindAsync_ShouldReturnNotEmpty()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u => u.Role).Create();
            await _appDbContext.Users.AddAsync(mockData);
            await _appDbContext.SaveChangesAsync();
            var result = await _genericRepository.FindAsync(x=>x.UserName==mockData.UserName);

            result.Should().NotBeEmpty();
           
        }


        [Fact]
        public async Task GenericRepository_FindAsync_ShouldReturnEmpty()
        {
            var result = await _genericRepository.FindAsync(x => x.UserName == string.Empty);

            result.Should().BeEmpty();

        }

        [Fact]
        public async Task GenericRepository_AddAsync_ShouldReturnSuccess()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u => u.Role).Create();
            await _appDbContext.Users.AddAsync(mockData);
            var result=await _appDbContext.SaveChangesAsync();

            result.Should().Be(1);

        }



        [Fact]
        public async Task GenericRepository_AddRangeAsync_ShouldReturnSuccess()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u => u.Role).CreateMany(10).ToList();
            await _appDbContext.Users.AddRangeAsync(mockData);
            var result = await _appDbContext.SaveChangesAsync();

            result.Should().Be(10);

        }

        [Fact]
        public async Task GenericRepository_AddRangeAsync_ShouldReturnFail()
        {
            await _appDbContext.Users.AddRangeAsync(new List<User>());
            var result = await _appDbContext.SaveChangesAsync();

            result.Should().Be(0);

        }

        [Fact]
        public async Task GenericRepository_RemoveRange_ShouldReturnSuccess()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u => u.Role).CreateMany(10).ToList();
            await _appDbContext.Users.AddRangeAsync(mockData);
            await _appDbContext.SaveChangesAsync();

            _genericRepository.RemoveRange(mockData);
            var result = await _appDbContext.SaveChangesAsync();

            result.Should().Be(10);

        }


        [Fact]
        public async Task GenericRepository_Remove_ShouldReturnSuccess()
        {
            var mockData = _fixture.Build<User>().Without(u => u.Notes).Without(u => u.Role).Create();
            _appDbContext.Users.Add(mockData);
            await _appDbContext.SaveChangesAsync();

            _genericRepository.Remove(mockData);
            var result = await _appDbContext.SaveChangesAsync();

            result.Should().Be(1);

        }



        [Fact]
        public async Task GenericRepository_RemoveRange_ShouldReturnFail()
        {

           _genericRepository.RemoveRange(new List<User>());
            var result = await _appDbContext.SaveChangesAsync();

            result.Should().Be(0);

        }

    }
}
