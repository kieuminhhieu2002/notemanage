﻿using Application.Repositories;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Infrastructures.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Tests.Repositories
{
    public class UserRepositoryTest : SetupTest
    {
        private readonly IUserRepository _userRepository;
        public UserRepositoryTest()
        {
            _userRepository = new UserRepository(_appDbContext, _claimsServiceMock.Object, _currentTimeMock.Object);
        }

        [Fact]
        public async Task GetUserLoginAsync_ReturnCorrectData()
        {
            //Arrange
            var userMock = _fixture.Create<User>();
            await _appDbContext.AddAsync(userMock);
            await _appDbContext.SaveChangesAsync();

            //Act
            var result = await _userRepository.GetUserLoginAsync(userMock.UserName, userMock.Password);

            //Assert
            result.Id.Should().Be(userMock.Id);
        }

    }
}
