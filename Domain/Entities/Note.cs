﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Note:BaseEntity
    {
        public string Title { get; set; }
        public string Contents { get; set; }

        
        public Guid UserId { get; set; }
        //[JsonIgnore]
        public User User { get; set; }

        public override string? ToString() =>base.ToString() + $"\t| {Title} \t| {Contents} \t| {UserId}";
    }
}
