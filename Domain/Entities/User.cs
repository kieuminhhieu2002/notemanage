﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class User:BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public string AvatarURL { get; set; }

        //Map to Role table
        public int RoleID { get; set; }
        public Role Role { get; set; }

        //JWT Refresh Token
        public string? RefreshToken { get; set; }
        public DateTime? RefreshTokenExpiryTime { get; set; }
     
        //Map to Note table
        public ICollection<Note> Notes { get; set; }

        public override string? ToString() => $"{UserName}\t| {Password}\t| {Status} \t| {FullName}";
    }
}
