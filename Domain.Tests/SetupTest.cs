using Application.Interface;
using Application.Interfaces;
using Application.Repositories;
using AutoFixture;
using AutoMapper;
using FluentValidation;
using Global.Shared.ViewModels;
using Infrastructures;
using Infrastructures.Mappers;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace Domain.Tests
{
    public class SetupTest:IDisposable
    {
        protected readonly IMapper _mapperConfig;
        protected readonly Fixture _fixture;
        protected readonly Mock<IValidator<UserViewModel>> _validator;
        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly Mock<IUserService> _userServiceMock;
        protected readonly Mock<IClaimsService> _claimsServiceMock;
        protected readonly Mock<ICurrentTime> _currentTimeMock;
        protected readonly Mock<IUserRepository> _userRepositoryMock;
        protected readonly Mock<INoteService> _noteServiceMock;
        protected readonly Mock<INoteRepository> _noteRepositoryMock;
        protected readonly Mock<IAuthenticationService> _authenticationServiceMock;
        protected readonly AppDbContext _appDbContext;
        public SetupTest()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            _mapperConfig = mappingConfig.CreateMapper();
            _fixture = new Fixture();
            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
    .ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _userServiceMock=new Mock<IUserService>();
            _claimsServiceMock = new Mock<IClaimsService>();
            _currentTimeMock = new Mock<ICurrentTime>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _noteServiceMock=new Mock<INoteService>();
            _noteRepositoryMock = new Mock<INoteRepository>();
            _authenticationServiceMock = new Mock<IAuthenticationService>();
            
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            _appDbContext = new AppDbContext(options);
            _validator = new Mock<IValidator<UserViewModel>>();
            _currentTimeMock.Setup(x=>x.GetCurrentTime()).Returns (DateTime.Now);
            _claimsServiceMock.Setup(x => x.GetCurrentUserId).Returns(Guid.Empty);
        }
        public void Dispose()
        {
            _appDbContext.Dispose();
        }
    }
}