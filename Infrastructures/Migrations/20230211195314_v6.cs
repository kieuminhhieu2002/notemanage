﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class v6 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("0ce988a9-35fc-41e9-80d1-0d3fe9480c95"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("ca62cfcc-bbde-41a4-ba90-f8158eeb024f"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(2817),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(5191));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(3468),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(6026));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("74395b0a-97c7-45ca-b5b9-bfc2416608fb"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" },
                    { new Guid("9b67b8a8-bf71-4564-bb39-3830c68af960"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("74395b0a-97c7-45ca-b5b9-bfc2416608fb"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("9b67b8a8-bf71-4564-bb39-3830c68af960"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(5191),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(2817));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(6026),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(3468));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("0ce988a9-35fc-41e9-80d1-0d3fe9480c95"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" },
                    { new Guid("ca62cfcc-bbde-41a4-ba90-f8158eeb024f"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" }
                });
        }
    }
}
