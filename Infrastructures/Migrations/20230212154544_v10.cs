﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class v10 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("1bf3b295-a528-4c3c-b8c7-3ffff59b91e4"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("48adc817-2441-4ad7-bc88-09cae4d6f999"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 22, 45, 44, 485, DateTimeKind.Local).AddTicks(4989),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 15, 37, 34, 916, DateTimeKind.Local).AddTicks(2347));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 22, 45, 44, 485, DateTimeKind.Local).AddTicks(5935),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 15, 37, 34, 916, DateTimeKind.Local).AddTicks(2990));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("39dd8234-4b3d-4567-8eb6-83be39c8b0db"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" },
                    { new Guid("713ba896-5fe5-41ad-a3de-9056d3135f8d"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("39dd8234-4b3d-4567-8eb6-83be39c8b0db"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("713ba896-5fe5-41ad-a3de-9056d3135f8d"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 15, 37, 34, 916, DateTimeKind.Local).AddTicks(2347),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 22, 45, 44, 485, DateTimeKind.Local).AddTicks(4989));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 15, 37, 34, 916, DateTimeKind.Local).AddTicks(2990),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 22, 45, 44, 485, DateTimeKind.Local).AddTicks(5935));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("1bf3b295-a528-4c3c-b8c7-3ffff59b91e4"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" },
                    { new Guid("48adc817-2441-4ad7-bc88-09cae4d6f999"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" }
                });
        }
    }
}
