﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class v5 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("07a48d8d-e08b-4000-abcb-5ae6b90e4ec3"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("2d54f50e-c854-4038-b08c-266c2b9061e6"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(5191),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 11, 23, 16, 35, 562, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(6026),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 11, 23, 16, 35, 562, DateTimeKind.Local).AddTicks(7198));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("0ce988a9-35fc-41e9-80d1-0d3fe9480c95"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" },
                    { new Guid("ca62cfcc-bbde-41a4-ba90-f8158eeb024f"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("0ce988a9-35fc-41e9-80d1-0d3fe9480c95"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("ca62cfcc-bbde-41a4-ba90-f8158eeb024f"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 11, 23, 16, 35, 562, DateTimeKind.Local).AddTicks(6328),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(5191));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 11, 23, 16, 35, 562, DateTimeKind.Local).AddTicks(7198),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 1, 46, 25, 740, DateTimeKind.Local).AddTicks(6026));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("07a48d8d-e08b-4000-abcb-5ae6b90e4ec3"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" },
                    { new Guid("2d54f50e-c854-4038-b08c-266c2b9061e6"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" }
                });
        }
    }
}
