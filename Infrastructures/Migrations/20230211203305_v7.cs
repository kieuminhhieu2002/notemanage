﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class v7 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("74395b0a-97c7-45ca-b5b9-bfc2416608fb"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("9b67b8a8-bf71-4564-bb39-3830c68af960"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 3, 33, 5, 117, DateTimeKind.Local).AddTicks(6977),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(2817));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 3, 33, 5, 117, DateTimeKind.Local).AddTicks(7999),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(3468));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("d7270945-9024-48cb-aeb1-4331485aa20f"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" },
                    { new Guid("eb65064d-5521-4d22-afad-3eb0babf7697"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("d7270945-9024-48cb-aeb1-4331485aa20f"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("eb65064d-5521-4d22-afad-3eb0babf7697"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Users",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(2817),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 3, 33, 5, 117, DateTimeKind.Local).AddTicks(6977));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notes",
                type: "datetime2",
                nullable: true,
                defaultValue: new DateTime(2023, 2, 12, 2, 53, 14, 202, DateTimeKind.Local).AddTicks(3468),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true,
                oldDefaultValue: new DateTime(2023, 2, 12, 3, 33, 5, 117, DateTimeKind.Local).AddTicks(7999));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarURL", "Email", "FullName", "LastAccessedDate", "Password", "RefreshToken", "RefreshTokenExpiryTime", "RoleID", "Status", "UserName" },
                values: new object[,]
                {
                    { new Guid("74395b0a-97c7-45ca-b5b9-bfc2416608fb"), "Demo.png", "User@gmail.com", "UserThanYeu", null, "@Abcaz12345", null, null, 2, "Actived", "Chubedan" },
                    { new Guid("9b67b8a8-bf71-4564-bb39-3830c68af960"), "Demo.png", "Admin@gmail.com", "AdminThanYeu", null, "pwd", null, null, 1, "Actived", "admin" }
                });
        }
    }
}
