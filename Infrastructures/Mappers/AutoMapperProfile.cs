﻿using AutoMapper;
using Domain.Entities;
using Global.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Mappers
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserViewModel, User>()
                //.ForMember(destination => destination.Id, options => options.MapFrom(source => source.Id))
                .ForMember(destination => destination.UserName, options => options.MapFrom(source => source.UserName))
                .ForMember(destination => destination.Password, options => options.MapFrom(source => source.Password))
                .ForMember(destination => destination.Email, options => options.MapFrom(source => source.Email))
                .ForMember(destination => destination.FullName, options => options.MapFrom(source => source.FullName))
                .ReverseMap();
            CreateMap<NoteViewModel, Note>()
                .ForMember(destination => destination.Contents, options => options.MapFrom(source => source.Contents))
                .ForMember(destination => destination.Title, options => options.MapFrom(source => source.Title))
                .ForMember(destination => destination.UserId, options => options.MapFrom(source => source.UserId))
                .ReverseMap();

            CreateMap<NoteUpdateModel, Note>()
                .ForMember(destination => destination.Id, options => options.MapFrom(source => source.Id))
                .ForMember(destination => destination.Contents, options => options.MapFrom(source => source.Contents))
                .ForMember(destination => destination.Title, options => options.MapFrom(source => source.Title))
                .ForMember(destination => destination.UserId, options => options.MapFrom(source => source.UserId))
                .ReverseMap();

        }
    }
}
