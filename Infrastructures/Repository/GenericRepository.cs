﻿using Domain.Entities;
using Application.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Application.Interface;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Infrastructures.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _appDbContext;
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimsService;
        public GenericRepository(AppDbContext appDbContext, IClaimsService claimsService,ICurrentTime currentTime)
        {
            _appDbContext = appDbContext;
            _claimsService = claimsService;
            _currentTime = currentTime;
        }

        public async Task<T> AddAsync(T entity)
        {
            entity.CreatedDate = DateTime.Now;
             var result = await _appDbContext.Set<T>().AddAsync(entity);
            return result.Entity;
        }

        public async Task AddRangeAsync(List<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreatedDate = DateTime.Now;
            }
            await _appDbContext.Set<T>().AddRangeAsync(entities);
        }

        public async Task<List<T>> FindAsync(Expression<Func<T, bool>> expression) => await _appDbContext.Set<T>().Where(expression).ToListAsync();

        public async Task<List<T>> GetAllAsync( params Expression<Func<T, object>>[] includes)
        {
            return await includes
           .Aggregate(_appDbContext.Set<T>().AsQueryable(),
               (entity, property) => entity.Include(property))
           .ToListAsync();
        }


        public async Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includes)
        {
            return await includes
           .Aggregate(_appDbContext.Set<T>().AsQueryable(),
               (entity, property) => entity.Include(property))
           .AsNoTracking()
           .FirstOrDefaultAsync(x=>x.Id==id);
        }

        public void Remove(T entity)
        {
            _appDbContext.Set<T>().Attach(entity);
            _appDbContext.Set<T>().Entry(entity).State = EntityState.Deleted;
        }

        public void RemoveRange(List<T> entities)
        {
            _appDbContext.Set<T>().RemoveRange(entities);
        }

        public void Update(T entity)
        {
            _appDbContext.Set<T>().Attach(entity);
            _appDbContext.Set<T>().Entry(entity).State=EntityState.Modified;
        }

        public void UpdateRange(List<T> entities)
        {
            _appDbContext.Set<T>().UpdateRange(entities);
        }


    }
}
