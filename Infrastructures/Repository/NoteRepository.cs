﻿using Domain.Entities;
using Application.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interface;

namespace Infrastructures.Repository
{
    public class NoteRepository : GenericRepository<Note>, INoteRepository
    {
        private readonly AppDbContext _appDbContext;
        public NoteRepository(AppDbContext appDbContext,IClaimsService claimsService, ICurrentTime currentTime) : base(appDbContext, claimsService, currentTime)
        {
            _appDbContext = appDbContext;
        }
    }
}
