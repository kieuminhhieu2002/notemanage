﻿using Application.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        private readonly IUserRepository _userRepository;
        private readonly INoteRepository _noteRepository;
        public UnitOfWork(AppDbContext appDbContext, IUserRepository userRepository, INoteRepository noteRepository)
        {
            _appDbContext = appDbContext;
            _userRepository = userRepository;
            _noteRepository = noteRepository;
        }
        public IUserRepository UserRepo { get=>_userRepository; }


        public INoteRepository NoteRepo { get=>_noteRepository; }

        public void Dispose()
        {
            _appDbContext.Dispose();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await _appDbContext.SaveChangesAsync();
        }


    }
}
