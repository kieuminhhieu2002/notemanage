﻿using Domain.Entities;
using Application.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interface;

namespace Infrastructures.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _appDbContext;
        public UserRepository(AppDbContext appDbContext, IClaimsService claimsService, ICurrentTime currentTime) : base(appDbContext,claimsService,currentTime)
        {
            _appDbContext = appDbContext;
        }

        public async Task<User> GetUserLoginAsync(string userName, string password)
            => await _appDbContext.Users.Include("Role").FirstOrDefaultAsync(u => u.UserName == userName & u.Password == password);
    }
}
