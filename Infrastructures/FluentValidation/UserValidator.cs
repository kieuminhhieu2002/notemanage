﻿using Domain.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentValidation
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(u => u.UserName).Length(5, 20).Matches("^[a-zA-Z]+$");
            //RuleFor(u => u.Id).NotEmpty().NotNull();
            RuleFor(u => u.Email).NotEmpty().NotNull().EmailAddress();
            RuleFor(u => u.FullName).NotNull().NotEmpty().Length(5, 50).Matches("^\\w+$");
            RuleFor(u => u.Password).NotEmpty().NotNull().Length(8, 20).Matches("^(!|@|#|$|%|^|&){1}[A-Z]{1}\\w+$");

        }
    }
}
