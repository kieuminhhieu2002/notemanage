﻿using Application.Interface;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using FluentValidation;
using Global.Shared.ViewModels;
using Infrastructures.FluentValidation;
using Infrastructures.Mappers;
using Infrastructures.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Text.Json.Serialization;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(
                option => option.UseSqlServer("Data Source=.;Initial Catalog=testdb3;Integrated Security=True;Trust Server Certificate=true")
                );
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<INoteRepository, NoteRepository>();
            services.AddScoped<ICurrentTime, CurrentTime>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<INoteService, NoteService>();
            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            services.AddScoped<IValidator<UserViewModel>,UserViewModelValidator>();

            return services;
        }
    }
}
