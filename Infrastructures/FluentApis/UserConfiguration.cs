﻿using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentApis
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.HasData(new User { Id = Guid.NewGuid(), UserName = "admin", FullName = "AdminThanYeu", Password = "pwd", Email = "Admin@gmail.com", RoleID = 1, Status = Convert.ToString(StatusEnum.Actived),AvatarURL="Demo.png" });
            builder.HasData(new User { Id = Guid.NewGuid(), UserName = "Chubedan", FullName = "UserThanYeu", Password = "@Abcaz12345", Email = "User@gmail.com", RoleID = 2, Status = Convert.ToString(StatusEnum.Actived), AvatarURL = "Demo.png" });
            builder.Property(u=>u.Status).HasDefaultValue(Convert.ToString(StatusEnum.Actived));
            builder.Property(u => u.RoleID).HasDefaultValue(2);
            builder.Property(u=>u.CreatedDate).HasDefaultValue(DateTime.Now);
        }
    }
}
