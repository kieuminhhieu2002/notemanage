﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.Common
{
    public class TokenModel
    {
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }

    }
}
