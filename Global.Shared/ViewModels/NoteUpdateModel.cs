﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.ViewModels
{
    public class NoteUpdateModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Contents { get; set; }
        public Guid UserId { get; set; }
    }
}
