﻿using Application.Interface;
using Application.Services;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Tests.Service
{
    public class CurrentTimeServiceTest
    {
        private readonly ICurrentTime _currentTime;
        public CurrentTimeServiceTest()
        {
            _currentTime = new CurrentTime();
        }

        [Fact]
        public async Task GetCurrentTime_ShouldReturnTimeExaclyToTheMiliSec()
        {
            //Arrange
            var expectedTime = DateTime.Now;

            //Act
            var result = _currentTime.GetCurrentTime();

            //Assert
            TimeSpan different = result - expectedTime;
            var timeDiffLessThan1MiliSec = different < TimeSpan.FromMilliseconds(1);
            timeDiffLessThan1MiliSec.Should().BeTrue();
        }
    }
}
