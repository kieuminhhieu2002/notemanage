﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.ViewModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Tests.Service
{
    public class NoteServiceTest:SetupTest
    {
        private readonly INoteService _noteService;
        public NoteServiceTest()
        {
            _noteService = new NoteService
            (
                _unitOfWorkMock.Object,
                _mapperConfig
            );
        }
        [Fact]
        public async Task CreateNote_ReturnCorrectData()
        {
            var noteMock = _fixture.Build<NoteViewModel>().Create();
            var note= _mapperConfig.Map<Note>( noteMock );

            _unitOfWorkMock.Setup(s => s.NoteRepo.AddAsync(note)).ReturnsAsync(note);
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).Verifiable();

            //Act
            var result = await _noteService.CreateNote(noteMock);

            //Assert
            result.Should().Be(result);
        }

        [Fact]
        public async Task DeleteNote_ReturnTrue()
        {
            //Arrange
            var note = _fixture.Build<Note>().Create();
            _unitOfWorkMock.Setup(s=>s.NoteRepo.GetByIdAsync(note.Id)).ReturnsAsync(note);
            _unitOfWorkMock.Setup(s => s.NoteRepo.Remove(note)).Verifiable();
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).ReturnsAsync(1);

            //Act
            var result = await _noteService.DeleteNote(note.Id);
            result.Should().BeTrue();
        }

        [Fact]
        public async Task DeleteNote_ReturnFasle_WhenNoteNull()
        {
            //Arrange
            var note = _fixture.Build<Note>().Create();
            Note noteReturn = null;
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetByIdAsync(note.Id)).ReturnsAsync(noteReturn);
            //Act
            var result = await _noteService.DeleteNote(note.Id);
            result.Should().BeFalse();
        }

        [Fact]
        public async Task DeleteNote_ReturnFasle_WhenSaveChangeFasle()
        {
            //Arrange
            var note = _fixture.Build<Note>().Create();
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetByIdAsync(note.Id)).ReturnsAsync(note);
            _unitOfWorkMock.Setup(s => s.NoteRepo.Remove(note)).Verifiable();
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).ReturnsAsync(0);

            //Act
            var result = await _noteService.DeleteNote(note.Id);
            result.Should().BeFalse();
        }


        [Fact]
        public async Task GetAllNotes_ReturnCorrectData()
        {
            var mockData = _fixture.Build<Note>().CreateMany(10).ToList();
            _unitOfWorkMock.Setup(s=>s.NoteRepo.GetAllAsync()).ReturnsAsync(mockData);

            //Act
            var result= await _noteService.GetAllNotes();
            //
            result.Should().BeEquivalentTo(mockData);
        }

        [Fact]
        public async Task GetNote_ReturnCorrectData()
        {
            var mockData = _fixture.Build<Note>().Create();
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetByIdAsync(mockData.Id)).ReturnsAsync(mockData);
            var noteMap = _mapperConfig.Map<NoteViewModel>(mockData);
            //Act
            var result = await _noteService.GetNote(mockData.Id);
            //
            result.Should().BeEquivalentTo(noteMap);
        }

        [Fact]
        public async Task UpdateNote_ReturnTrue()
        {
            var noteView = _fixture.Build<NoteUpdateModel>().Create();
            var note = _fixture.Build<Note>().Create();
            note.Id = noteView.Id;
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetByIdAsync(noteView.Id)).ReturnsAsync(note);
            note= _mapperConfig.Map<Note>(noteView);

            _unitOfWorkMock.Setup(s => s.NoteRepo.Update(note)).Verifiable();
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).ReturnsAsync(1);

            //Act
            var result = await _noteService.UpdateNote(noteView);

            //Assert
            result.Should().BeTrue();

        }


        [Fact]
        public async Task UpdateNote_ReturnFasle_WhenNoteNull()
        {
            var noteView = _fixture.Build<NoteUpdateModel>().Create();
            Note note = null;
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetByIdAsync(noteView.Id)).ReturnsAsync(note);
          
            //Act
            var result = await _noteService.UpdateNote(noteView);

            //Assert
            result.Should().BeFalse();

        }

        [Fact]
        public async Task UpdateNote_ReturnFalse_WhenSaveChangeFalse()
        {
            var noteView = _fixture.Build<NoteUpdateModel>().Create();
            var note = _fixture.Build<Note>().Create();
            note.Id = noteView.Id;
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetByIdAsync(noteView.Id)).ReturnsAsync(note);
            note = _mapperConfig.Map<Note>(noteView);

            _unitOfWorkMock.Setup(s => s.NoteRepo.Update(note)).Verifiable();
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).ReturnsAsync(0);

            //Act
            var result = await _noteService.UpdateNote(noteView);

            //Assert
            result.Should().BeFalse();

        }

        [Fact]
        public async Task ExportToExcel_ReturnTrue()
        {
            var notes = _fixture.Build<Note>().CreateMany(10).ToList();
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetAllAsync(n => n.User)).ReturnsAsync(notes);

            var result = await _noteService.ExportToExcel();
            result.Should().BeTrue();
        }

        [Fact]
        public async Task ExportToExcel_ReturnFalse_WhenNotesIsNull()
        {
            var notes = _fixture.Build<Note>().CreateMany(10).ToList();
            _unitOfWorkMock.Setup(s => s.NoteRepo.GetAllAsync(n => n.User)).ReturnsAsync(notes=null);

            var result = await _noteService.ExportToExcel();
            result.Should().BeFalse();
        }
    }
}
