﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.ViewModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using System.Drawing.Imaging;
using System.Drawing;
using Microsoft.Extensions.Hosting;
using System.IO;
using Domain.Enums;

namespace Applications.Tests.Service
{
    public class UserServiceTests:SetupTest
    {
        private readonly IUserService _userService;

        public UserServiceTests()
        {
            _userService = new UserService(
                _unitOfWorkMock.Object,
                _mapperConfig,
                _validator.Object
                );
        }

        [Fact]
        public async Task GetListUserAsync_ReturnSuccess()
        {
            //Arrange
            var mockDatas = _fixture.Build<User>().CreateMany(10).ToList();
            _unitOfWorkMock.Setup(x=>x.UserRepo.GetAllAsync(u => u.Role, u => u.Notes)).ReturnsAsync(mockDatas);
            //act
            var result = await _userService.GetListUserAsync();
            //Assert
            result.Should().BeEquivalentTo(mockDatas);
        }

        [Fact]
        public async Task GetListUserAsync_ReturnNull()
        {
            //Arrange
            List<User> mockDatas = null;
            _unitOfWorkMock.Setup(x => x.UserRepo.GetAllAsync(u => u.Role, u => u.Notes)).ReturnsAsync(mockDatas);
            //act
            var result = await _userService.GetListUserAsync();
            //Assert
            result.Should().BeNull();
        }

        [Fact]
        public async Task Register_ReturnTrue()
        {
            //Arrange
            var mockData = _fixture.Build<UserViewModel>().Create();
            List<User> users = new List<User>();
            //ValidationResult validResult = new ValidationResult();
            _validator.Setup(x => x.ValidateAsync(It.IsAny<UserViewModel>(), It.IsAny<CancellationToken>()))
                             .ReturnsAsync(new ValidationResult());
            _unitOfWorkMock.Setup(s=>s.UserRepo.FindAsync(u => u.UserName == mockData.UserName || u.Email == mockData.Email))
                .ReturnsAsync(users);
            var userMapper = _mapperConfig.Map<User>(mockData);
            _unitOfWorkMock.Setup(x => x.UserRepo.AddAsync(userMapper))
                .ReturnsAsync(userMapper);
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(1);
            //act
            var result = await _userService.Register(mockData);
            //Assert
            result.Should().Be(true);
        }
        [Fact]
        public async Task Register_ReturnFalse_WhenValidatorInValid()
        {
            //Arrange
            var mockData = _fixture.Build<UserViewModel>().Create();
            
            ValidationResult validResult = new ValidationResult();
            validResult.Errors.Add(new ValidationFailure());
            _validator.Setup(x => x.ValidateAsync(It.IsAny<UserViewModel>(), It.IsAny<CancellationToken>()))
                             .ReturnsAsync(validResult);

          
            //act
            var result = await _userService.Register(mockData);
            //Assert
            result.Should().Be(false);
        }
        [Fact]
        public async Task Register_ReturnFalse_WhenUserExist()
        {
            //Arrange
            var mockData = _fixture.Build<UserViewModel>().Create();
            List<User> users = _fixture.Build<User>().CreateMany(1).ToList();
            ValidationResult validResult = new ValidationResult();
            validResult.Errors.Add(new ValidationFailure());
            _validator.Setup(x => x.ValidateAsync(It.IsAny<UserViewModel>(), It.IsAny<CancellationToken>()))
                             .ReturnsAsync(validResult);

            _unitOfWorkMock.Setup(s => s.UserRepo.FindAsync(u => u.UserName == mockData.UserName || u.Email == mockData.Email))
              .ReturnsAsync(users);
            //act
            var result = await _userService.Register(mockData);
            //Assert
            result.Should().Be(false);
        }

        [Fact]
        public async Task Register_ReturnFalse_WhenSaveChangeFalse()
        {
            //Arrange
            var mockData = _fixture.Build<UserViewModel>().Create();
            List<User> users = new List<User>();
            //ValidationResult validResult = new ValidationResult();
            _validator.Setup(x => x.ValidateAsync(It.IsAny<UserViewModel>(), It.IsAny<CancellationToken>()))
                             .ReturnsAsync(new ValidationResult());
            _unitOfWorkMock.Setup(s => s.UserRepo.FindAsync(u => u.UserName == mockData.UserName || u.Email == mockData.Email))
                .ReturnsAsync(users);
            var userMapper = _mapperConfig.Map<User>(mockData);
            _unitOfWorkMock.Setup(x => x.UserRepo.AddAsync(userMapper))
                .ReturnsAsync(userMapper);
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(0);
            //act
            var result = await _userService.Register(mockData);
            //Assert
            result.Should().Be(false);
        }

        [Fact]
        public async Task UpdateToAdmin_ReturnSuccess()
        {
            //Arrange
            var mockData= _fixture.Build<User>().Create();
            _unitOfWorkMock.Setup(s => s.UserRepo.GetByIdAsync(mockData.Id)).ReturnsAsync(mockData);
            mockData.RoleID = 1;
            _unitOfWorkMock.Setup(s=>s.UserRepo.Update(mockData)).Verifiable();
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).ReturnsAsync(1);

            //Act
            var result = await _userService.UpdateToAdmin(mockData.Id);

            //Assert

            result.Should().Be(true);

        }
        [Fact]
        public async Task UpdateToAdmin_ReturnFalse_WhenUserIsNull()
        {
            //Arrange
            var mockData = _fixture.Build<User>().Create();
            _unitOfWorkMock.Setup(s => s.UserRepo.GetByIdAsync(mockData.Id)).ReturnsAsync(new User());
            

            //Act
            var result = await _userService.UpdateToAdmin(mockData.Id);

            //Assert

            result.Should().Be(false);

        }

        [Fact]
        public async Task UpdateToAdmin_ReturnFalse_WhenSaveChangeFalse()
        {
            //Arrange
            var mockData = _fixture.Build<User>().Create();
            _unitOfWorkMock.Setup(s => s.UserRepo.GetByIdAsync(mockData.Id)).ReturnsAsync(new User());

            mockData.RoleID = 1;
            _unitOfWorkMock.Setup(s => s.UserRepo.Update(mockData)).Verifiable();
            _unitOfWorkMock.Setup(s => s.SaveChangeAsync()).ReturnsAsync(0);
            //Act
            var result = await _userService.UpdateToAdmin(mockData.Id);

            //Assert

            result.Should().Be(false);

        }

        [Fact]
        public async Task UploadFile_ReturnTrue()
        {
            //Arrange
            var fileMock = new Mock<IFormFile>();
            var fileName = "test.pdf";
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(f=>f.CopyToAsync(It.IsAny<FileStream>(),It.IsAny<CancellationToken>())).Verifiable();
            var userMock= _fixture.Build<User>().Create();

            _unitOfWorkMock.Setup(s=>s.UserRepo.GetByIdAsync(userMock.Id)).ReturnsAsync(userMock);
            _unitOfWorkMock.Setup(s=>s.UserRepo.Update(userMock)).Verifiable();

            _unitOfWorkMock.Setup(s=>s.SaveChangeAsync()).ReturnsAsync(1);

            //Act
            var result = await _userService.UpLoadFile(userMock.Id, fileMock.Object);
            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public async Task UploadFile_ReturnFalse_WhenFileNull()
        {
            //Arrange
            var fileMock = new Mock<IFormFile>();
            var userMock = _fixture.Build<User>().Create();

            //Act
            var result = await _userService.UpLoadFile(userMock.Id, null);
            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public async Task BlockAccount_ReturnTrue()
        {
            //Arrange
            var userMock = _fixture.Build<User>().Create();
            userMock.Status =Convert.ToString(StatusEnum.Actived);
            _unitOfWorkMock.Setup(u => u.UserRepo.GetByIdAsync(userMock.Id)).ReturnsAsync(userMock);
            _unitOfWorkMock.Setup(u => u.UserRepo.Update(userMock)).Verifiable();
            _unitOfWorkMock.Setup(u => u.SaveChangeAsync()).ReturnsAsync(1);
            //Act
            var result = await _userService.BlockAccount(userMock.Id);
            //Assert

            result.Should().BeTrue();
        }
        [Fact]
        public async Task BlockAccount_ReturnFalse_WhenUserNull()
        {
            //Arrange
            var userMock = _fixture.Build<User>().Create();
            userMock.Status = Convert.ToString(StatusEnum.Actived);
            User user = null; 
            _unitOfWorkMock.Setup(u => u.UserRepo.GetByIdAsync(userMock.Id)).ReturnsAsync(user);
            
            //Act
            var result = await _userService.BlockAccount(userMock.Id);
            //Assert

            result.Should().BeFalse();
        }

        [Fact]
        public async Task BlockAccount_ReturnFalse_WhenSaveChangeFalse()
        {
            //Arrange
            var userMock = _fixture.Build<User>().Create();
            userMock.Status = Convert.ToString(StatusEnum.Actived);
            _unitOfWorkMock.Setup(u => u.UserRepo.GetByIdAsync(userMock.Id)).ReturnsAsync(userMock);
            _unitOfWorkMock.Setup(u => u.UserRepo.Update(userMock)).Verifiable();
            _unitOfWorkMock.Setup(u => u.SaveChangeAsync()).ReturnsAsync(0);
            //Act
            var result = await _userService.BlockAccount(userMock.Id);
            //Assert

            result.Should().BeFalse();
        }
    }
}
