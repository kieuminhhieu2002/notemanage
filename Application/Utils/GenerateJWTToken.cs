﻿using Application.Interface;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Application.Utils
{
    public static class GenerateJWTToken
    {

        public static JwtSecurityToken GenerateJSONWebToken(this User userLogin,string key,string issuer,string audience,string timeExpire)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            _ = int.TryParse(timeExpire, out int tokenValidityInMinutes);

           
            var claims = new[]
            {
                new Claim("userId",userLogin.Id.ToString()),
                new Claim(ClaimTypes.Name,userLogin.UserName),
                new Claim(ClaimTypes.Role,userLogin.Role.RoleName),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())

            };
            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims,
                expires:DateTime.UtcNow.AddMinutes(tokenValidityInMinutes),
                signingCredentials:credentials);

            return token;
        }

        

        public static ClaimsPrincipal? GetPrincipalFromExpiredToken(this string? token,string key)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;

        }
    }
}
