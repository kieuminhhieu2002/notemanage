﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IUnitOfWork:IDisposable 
    {
        public IUserRepository UserRepo { get; }
        public INoteRepository NoteRepo { get; }
        public Task<int> SaveChangeAsync();
    }
}
