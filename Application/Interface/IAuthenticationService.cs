﻿using Global.Shared.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interface
{
    public interface IAuthenticationService
    {
        Task<TokenModel> Login(string userName, string password);
        Task<TokenModel> RefreshToken(TokenModel tokenModel);
        Task<bool> Logout();
    }
}
