﻿using Domain.Entities;
using Global.Shared.Common;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetListUserAsync();
        Task<bool> Register(UserViewModel user);

        Task<bool> BlockAccount(Guid id);

        Task<bool> UpdateToAdmin(Guid id);

        Task<bool> UpLoadFile(Guid id, IFormFile file);
    }
}
