﻿using Domain.Entities;
using Global.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface INoteService
    {
        Task<Note> CreateNote(NoteViewModel noteView);
        Task<bool> UpdateNote(NoteUpdateModel noteNew);
        Task<bool> DeleteNote(Guid id);
        Task<List<Note>> GetAllNotes();
        Task<NoteViewModel> GetNote(Guid id);

        Task<bool> ExportToExcel();
    }
}
