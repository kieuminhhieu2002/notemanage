﻿using Domain.Entities;
using Global.Shared.ViewModels;
using Application.Repositories;
using AutoMapper;
using FluentValidation;
using Global.Shared.Common;
using Application.Interfaces;
using Domain.Enums;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IValidator<UserViewModel> _validator;
  
        public UserService(IUnitOfWork unitOfWork,IMapper mapper, IValidator<UserViewModel> validator)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _validator = validator;

        }

        public async Task<bool> BlockAccount(Guid id)
        {
            var user= await _unitOfWork.UserRepo.GetByIdAsync(id);
            if (user == null) { return false; }
            user.Status = Convert.ToString(StatusEnum.Disable);
            _unitOfWork.UserRepo.Update(user);
            var check = await _unitOfWork.SaveChangeAsync()>0;
            return check;

        }

        public async Task<List<User>> GetListUserAsync() 
        {
            var user= await _unitOfWork.UserRepo.GetAllAsync( u=>u.Role,u=>u.Notes);
            //var userView=_mapper.Map<List<UserViewModel>>(user);
            
            return user;
        }
        public async Task<bool> Register(UserViewModel userView)
        {
            var userValidator = await _validator.ValidateAsync(userView);
            if (!userValidator.IsValid)
            {           
                return false;
            }
            var checkUser= await _unitOfWork.UserRepo.FindAsync(u=>u.UserName==userView.UserName || u.Email==userView.Email);
            if(checkUser.Count()!=0)
            {
                return false;
            }

            var userMapper = _mapper.Map<User>(userView);
            await _unitOfWork.UserRepo.AddAsync(userMapper);
            var result= await _unitOfWork.SaveChangeAsync()>0;
            if (result) return true;
            return false;

        }

        public async Task<bool> UpdateToAdmin(Guid id)
        {
            var user = await _unitOfWork.UserRepo.GetByIdAsync(id);
            if (user == null) { return false; }
            user.RoleID = 1;
            _unitOfWork.UserRepo.Update(user);
            var check = await _unitOfWork.SaveChangeAsync() > 0;
            return check;
        }

        public async Task<bool> UpLoadFile(Guid id, IFormFile file)
        {
            if(file== null)
            {
                return false;
            }
            var user = await _unitOfWork.UserRepo.GetByIdAsync(id);
            if (user == null) { return false; }
            user.AvatarURL = file.FileName;
            _unitOfWork.UserRepo.Update(user);
            //D:\C#\Work_Space\NoteManage\WebApi\Upload\Images\Event-Usecase.png
            string filePathName = Path.Combine("D:\\C#\\Work_Space\\NoteManage\\WebApi\\", "Upload\\Images", file.FileName);
            Console.WriteLine(filePathName);
            using (var stream = new FileStream(filePathName, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var check = await _unitOfWork.SaveChangeAsync() > 0;
            return check;
        }
    }
}
