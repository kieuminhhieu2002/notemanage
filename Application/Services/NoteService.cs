﻿using Application.Interfaces;
using Application.Repositories;
using AutoMapper;
using Domain.Entities;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class NoteService : INoteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public NoteService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

        public async Task<Note> CreateNote(NoteViewModel noteView)
        {
            var noteMapper = _mapper.Map<Note>(noteView);
             var result = await _unitOfWork.NoteRepo.AddAsync(noteMapper);

            await _unitOfWork.SaveChangeAsync();

            return result;
        }

        public async Task<bool> DeleteNote(Guid id)
        {
            var note = await _unitOfWork.NoteRepo.GetByIdAsync(id);
            if (note == null) return false;

            _unitOfWork.NoteRepo.Remove(note);
            var check = await _unitOfWork.SaveChangeAsync() > 0;
            return check;
        }

        public async Task<bool> ExportToExcel()
        {
            var notes= await _unitOfWork.NoteRepo.GetAllAsync(n=>n.User);
            if(notes.IsNullOrEmpty()) return false;
            var dataExport= (from note in notes
                             orderby note.Id descending
                             select new
                             {
                                 UserName = note.User.UserName,
                                 Note = note.ToString(),
                                 CreatedDate = note.CreatedDate
                             }).Take(10);
            
            string htmlString = "<table style=\"width: 800px;border: 1px solid;\"><thead><tr>";
            htmlString += "<th style=\"width:10%;text-align: left;\">User Name</th>";
            htmlString += "<th style=\"width:10%;text-align: left;\">Note</th>";
            htmlString += "<th style=\"width:10%;text-align: left;\">Created Date</th>";
            htmlString += "</tr></thead><tbody>";

            foreach (var item in dataExport)
            {
                htmlString += "<tr><td style=\"width: 10%;text-align: left;\">" + item.UserName + " </td>";
                htmlString += "<td style=\"width: 10%;text-align: left;\">" + item.Note + "</td>";
                htmlString += "<td style=\"width: 10%;text-align: left;\">" + item.CreatedDate + "</td></tr>";
            }

            htmlString += "</tbody></table>";
            string filePathName = Path.Combine("D:\\C#\\Work_Space\\NoteManage\\WebApi\\", "Upload\\Files", "New10Notes" + DateTime.Now.Ticks.ToString() + ".xls");
            System.IO.File.AppendAllText(filePathName, htmlString);
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(filePathName, out var contentType))
            {
                contentType = "application/octet-stream";
            }
            var bytes = await System.IO.File.ReadAllBytesAsync(filePathName);

            // File(bytes, contentType, Path.GetFileName(filePathName));

            return true;
        }

        public async Task<List<Note>> GetAllNotes()
        {
            return await _unitOfWork.NoteRepo.GetAllAsync();
        }

        public async Task<NoteViewModel> GetNote(Guid id)
        {
            var note = await _unitOfWork.NoteRepo.GetByIdAsync(id);
            var noteView= _mapper.Map<NoteViewModel>(note);
            return noteView;
        }

        public async Task<bool> UpdateNote(NoteUpdateModel noteNew)
        {
            var note = await _unitOfWork.NoteRepo.GetByIdAsync(noteNew.Id);
            if (note== null) return false;
            note = _mapper.Map<Note>(noteNew);
            _unitOfWork.NoteRepo.Update(note);

            var check = await _unitOfWork.SaveChangeAsync() > 0;
            return check;

            
        }
    }
}
